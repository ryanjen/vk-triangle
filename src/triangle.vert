#version 450

layout ( location = 0 ) in vec3 position;
layout ( location = 1 ) in vec3 color;

layout (set = 0, binding = 0) uniform Block {
  mat4 transformation; 
};

layout ( location = 0 ) out vec3 vert_color;

void main() {
  gl_Position = transformation * vec4(position, 1.0f);
  vert_color = color;
}