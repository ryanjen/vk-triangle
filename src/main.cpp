#include <chrono>
#include <cstdint>
#include <dlfcn.h>
#include <vulkan/vulkan_core.h>
#define VK_NO_PROTOTYPES 1
#include "VulkanFunctions.h"
#include <GLFW/glfw3.h>

#define _USE_MATH_DEFINES
#include <math.h>

#include <vector>
#include <iostream>
#include <cassert>
#include <cstring>

#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace VkTriangle {

static const uint32_t vertex_buffer_size = 512;
static const uint32_t vertex_stride = 6 * sizeof(float);
static float vertices[] = {
  0.0f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, //R
  -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, //G
  0.5f, 0.5f, 0.0f,  0.0f, 0.0f, 1.0f, //B
};

static const VkExtent2D window_size = { .width = 640, .height = 480 };

#define VK_ASSERT( expr ) assert( VK_SUCCESS == (expr) );

static const std::vector<const char *> desiredInstanceLayers = {
  "VK_LAYER_KHRONOS_validation"
};

static std::vector<const char *> desiredInstanceExtensions = {
  VK_KHR_SURFACE_EXTENSION_NAME,
};
static std::vector<const char *> desiredDeviceExtensions = {
  VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

// number of current frames to dispatch. Will take min with swapchain size
static const uint32_t maxFrames = 3;

class Context {
  public:
    Context();
    
    void init();
    void shutdown();
    void renderLoop();

  private:
    void * library;
    VkInstance instance;
    VkPhysicalDevice physicalDevice;
    uint32_t graphicsFamily;
    uint32_t presentFamily;
    VkDevice logicalDevice;
    VkQueue graphicsQueue;
    VkQueue presentQueue;


    struct SwapchainImageResources {
      VkImage image;
      VkImageView imageView;      
    };

    GLFWwindow* window;
    VkSurfaceKHR presentationSurface;
    VkSurfaceCapabilitiesKHR surfaceCapabilities;

    VkSwapchainKHR currentSwapchain;
    VkSurfaceFormatKHR swapchainFormat;    
    std::vector<SwapchainImageResources> swapchainImages;

    VkPhysicalDeviceMemoryProperties memoryProperties;
    VkBuffer vertexStagingBuffer;
    VkDeviceMemory vertexStagingMemory;
    VkBuffer vertexBuffer;
    VkDeviceMemory vertexBufferMemory;

    VkCommandPool transferPool;

    VkRenderPass renderpass;

    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;


    struct BufferResources {
      VkBuffer buffer;
      VkDeviceMemory memory;
      void *mappedData;
    };


    struct FrameResources {
      uint32_t imageIndex;
      VkCommandBuffer commandBuffer;
      VkDescriptorSet descriptorSet;
      BufferResources uniformBuffer;
      VkFramebuffer framebuffer;
      VkSemaphore imageAcquired;
      VkSemaphore presentable;
      VkFence finishedDrawing;
    };


    std::vector<FrameResources> frames;
    std::vector<VkCommandPool> frameCommandPools; 

    VkDescriptorSetLayout drawDescriptorSetLayout;
    VkDescriptorPool drawDescriptorPool;


    void initGLFW() {
      glfwInit();
      assert( glfwVulkanSupported() );

      uint32_t count;
      const char** extensions = glfwGetRequiredInstanceExtensions(&count);
      
      for (uint32_t i = 0; i < count; i++) {
        desiredInstanceExtensions.push_back(extensions[i]);
      }
    }

    void createWindow() {
      glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
      this->window = glfwCreateWindow(window_size.width, window_size.height, "Triangle", NULL, NULL);
      assert( this->window );
    }

    void createSurface() {
      VK_ASSERT( glfwCreateWindowSurface(instance, window, NULL, &this->presentationSurface) )
    }



    void loadGlobalFunctions() {
      this->library = dlopen( "libvulkan.so.1", RTLD_NOW );
      assert(this->library != nullptr);

      // load loader function from vulkan dynamic library
      #define EXPORTED_VULKAN_FUNCTION( name ) \
        name = (PFN_##name) dlsym( this->library, #name ); \
        assert( name != nullptr );

      // load global level functions
      #define GLOBAL_LEVEL_VULKAN_FUNCTION( name ) \
        name = (PFN_##name) vkGetInstanceProcAddr( nullptr, #name ); \
        assert( name != nullptr);

      #include "ListOfVulkanFunctions.inl"
    }


    void createInstance() {
      //VkPhysicalDeviceFeatures desiredDeviceFeatures = {};

      VkApplicationInfo applicationInfo = {
        VK_STRUCTURE_TYPE_APPLICATION_INFO,
        nullptr,
        "vk triangle",
        VK_MAKE_VERSION(1, 0, 0),
        "triangle engine",
        VK_MAKE_VERSION(1, 0, 0),
        VK_MAKE_VERSION(1, 0, 0)
      };

      VkInstanceCreateInfo instanceCreateInfo = {
        VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        nullptr,
        0,
        &applicationInfo,
        (uint32_t) desiredInstanceLayers.size(),
        desiredInstanceLayers.data(),
        (uint32_t) desiredInstanceExtensions.size(),
        desiredInstanceExtensions.size() > 0 ? &desiredInstanceExtensions[0] : nullptr
      };

      VK_ASSERT( vkCreateInstance(&instanceCreateInfo, nullptr, &this->instance));
    }

    void loadInstanceFunctions() {
      #define INSTANCE_LEVEL_VULKAN_FUNCTION( name ) \
        name = (PFN_##name) vkGetInstanceProcAddr( this->instance, #name ); \
        assert(name != nullptr);
      
      #include "ListOfVulkanFunctions.inl"

      #define INSTANCE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) \
        for (auto & enabled : desiredInstanceExtensions ) { \
          if ( !strcmp(enabled, extension ) ) { \
            name = (PFN_##name) vkGetInstanceProcAddr( this->instance, #name); \
            assert(name != nullptr); \
          } \
        }
      
      #include "ListOfVulkanFunctions.inl"
    }

    static std::vector<VkQueueFamilyProperties> getQueueFamilies(VkPhysicalDevice device) {
      uint32_t numFamilies;
      std::vector<VkQueueFamilyProperties> families;
      vkGetPhysicalDeviceQueueFamilyProperties(device, &numFamilies, nullptr);
      families.resize(numFamilies);
      vkGetPhysicalDeviceQueueFamilyProperties(device, &numFamilies, families.data());

      return families;
    }


    bool selectFamily(VkPhysicalDevice device, VkFlags flags, uint32_t & resultFamily) {
      std::vector<VkQueueFamilyProperties> families = getQueueFamilies(device);

      for (uint32_t i = 0; i < families.size(); i++) {
        auto & family = families[i];
        if ( family.queueCount > 0 && 
            (family.queueFlags & flags) == flags ) {
          resultFamily = i;
          return true;
        }
      }
      return false;
    }


    bool selectPresentFamily(VkPhysicalDevice device, uint32_t & resultFamily) {
      std::vector<VkQueueFamilyProperties> families = getQueueFamilies(device);

      for (uint32_t i = 0; i < families.size(); i++) {
        if ( glfwGetPhysicalDevicePresentationSupport(this->instance, device, i) ) {
          VkBool32 supported;
          VkResult res = vkGetPhysicalDeviceSurfaceSupportKHR(device, i, this->presentationSurface, &supported);
          if (res == VK_SUCCESS && supported) {
            resultFamily = i;
            return true;
          }
        }
      }
      return false;
    }


    bool choosePhysicalDeviceAndQueueFamilies() {
      std::vector<VkPhysicalDevice> devices;
      uint32_t numDevices;
      vkEnumeratePhysicalDevices(this->instance, &numDevices, nullptr);
      devices.resize(numDevices);
      vkEnumeratePhysicalDevices(this->instance, &numDevices, devices.data());

      for (VkPhysicalDevice currentDev: devices) {
        if ( !selectFamily(currentDev, VK_QUEUE_GRAPHICS_BIT, this->graphicsFamily) )
          continue;
        if ( !selectPresentFamily(currentDev, this->presentFamily))
          continue;

        this->physicalDevice = currentDev;
        
        return true;
      }
      return false;
    }


    void createLogicalDevice() {
      float prio[1];
      prio[0] = 1.0f;
      std::vector<VkDeviceQueueCreateInfo> queueInfos = {
        {
          .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
          .pNext = nullptr,
          .flags = 0,
          .queueFamilyIndex = graphicsFamily,
          .queueCount = 1,
          .pQueuePriorities = prio,
        }
      };

      if (graphicsFamily != presentFamily) {
        queueInfos.push_back({
        .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .queueFamilyIndex = presentFamily,
        .queueCount = 1,
        .pQueuePriorities = prio,
        });
      }


      VkPhysicalDeviceFeatures deviceFeatures = {};
      VkDeviceCreateInfo deviceCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .queueCreateInfoCount = (uint32_t) queueInfos.size(),
        .pQueueCreateInfos = queueInfos.data(),
        .enabledLayerCount = 0,
        .ppEnabledLayerNames = nullptr,
        .enabledExtensionCount = (uint32_t) desiredDeviceExtensions.size(),
        .ppEnabledExtensionNames = desiredDeviceExtensions.data(),
        .pEnabledFeatures = &deviceFeatures,
      };
      VK_ASSERT( vkCreateDevice(this->physicalDevice, &deviceCreateInfo, nullptr, &this->logicalDevice) );
    };


    void loadDeviceFunctions() {
      #define DEVICE_LEVEL_VULKAN_FUNCTION( name ) \
        name = (PFN_##name) vkGetDeviceProcAddr(this->logicalDevice, #name); \
        assert(name != nullptr);

      #define DEVICE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) \
        for (auto & enabled: desiredDeviceExtensions) { \
          if ( !strcmp(enabled, extension) ) { \
            name = (PFN_##name) vkGetDeviceProcAddr(this->logicalDevice, #name); \
            assert(name != nullptr); \
          } \
        }

      #include "ListOfVulkanFunctions.inl"
    }


    void getQueues() {
      vkGetDeviceQueue(this->logicalDevice, graphicsFamily, 0, &this->graphicsQueue);
      vkGetDeviceQueue(this->logicalDevice, presentFamily, 0, &this->presentQueue);
    }


    void getMemoryProperties() {
      vkGetPhysicalDeviceMemoryProperties(this->physicalDevice, &this->memoryProperties);
    }


    void createBuffer(VkMemoryPropertyFlags desiredProperties, VkBufferUsageFlags usage, VkDeviceSize size, VkBuffer & buffer, VkDeviceMemory & memoryObject) {
      VkBufferCreateInfo bufferCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .size = size,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = nullptr,
      };
      VK_ASSERT( vkCreateBuffer(this->logicalDevice, &bufferCreateInfo, nullptr, &buffer) );

      VkMemoryRequirements requirements;
      vkGetBufferMemoryRequirements(this->logicalDevice, buffer, &requirements);

      memoryObject = VK_NULL_HANDLE;
      
      uint32_t type;
      for (type = 0; type < this->memoryProperties.memoryTypeCount; type++) {
        if (requirements.memoryTypeBits & (1 << type) &&
            ( ( this->memoryProperties.memoryTypes[type].propertyFlags & desiredProperties) == desiredProperties ) ) {
          VkMemoryAllocateInfo allocInfo = {
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            .pNext = nullptr,
            .allocationSize = requirements.size,
            .memoryTypeIndex = type,
          };
          VkResult res = vkAllocateMemory(this->logicalDevice, &allocInfo, nullptr, &memoryObject);
          if (res == VK_SUCCESS)
            break;
        }
      }
      assert(memoryObject != VK_NULL_HANDLE);
      VK_ASSERT( vkBindBufferMemory(this->logicalDevice, buffer, memoryObject, 0) );
    }

    void createStagingBuffer() {
      createBuffer(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, vertex_buffer_size, this->vertexStagingBuffer, this->vertexStagingMemory);
    }

    void createVertexBuffer() {
      createBuffer(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, vertex_buffer_size,
              this->vertexBuffer, this->vertexBufferMemory);
    }


    void uploadToVertexStagingBuffer() {
      assert( sizeof(vertices) <= vertex_buffer_size );
      void *data;
      VK_ASSERT( vkMapMemory(this->logicalDevice, this->vertexStagingMemory, 0, vertex_buffer_size, 0, &data) );
      memcpy(data, vertices, sizeof(vertices));
      VkMappedMemoryRange range = {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .pNext = nullptr,
        .memory = this->vertexStagingMemory,
        .offset = 0,
        .size = vertex_buffer_size
      };
      VK_ASSERT( vkFlushMappedMemoryRanges(this->logicalDevice, 1, &range) );
    }


    void oneTimeVertexTransfer() {
      VkCommandPoolCreateInfo poolInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .queueFamilyIndex = graphicsFamily
      };
      VK_ASSERT( vkCreateCommandPool(this->logicalDevice, &poolInfo, nullptr, &this->transferPool) );
      VkCommandBufferAllocateInfo cmdBufferInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = nullptr,
        .commandPool = this->transferPool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
      };

      VkCommandBuffer transferCmdBuffer;
      VK_ASSERT( vkAllocateCommandBuffers(this->logicalDevice, &cmdBufferInfo, &transferCmdBuffer) );

      VkCommandBufferBeginInfo beginInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = nullptr,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        .pInheritanceInfo = nullptr,
      };

      ///////////////// start recording
      VK_ASSERT( vkBeginCommandBuffer(transferCmdBuffer, &beginInfo) );

      VkBufferCopy region = {
        .srcOffset = 0,
        .dstOffset = 0,
        .size = vertex_buffer_size,
      };
      vkCmdCopyBuffer(transferCmdBuffer, this->vertexStagingBuffer, this->vertexBuffer, 1, &region);
      
      VkBufferMemoryBarrier vertBufferBarrier = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
        .pNext = nullptr,
        .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .buffer = vertexBuffer,
        .offset = 0,
        .size = VK_WHOLE_SIZE,
      };
      vkCmdPipelineBarrier(transferCmdBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, 0,
              0, nullptr, 1, &vertBufferBarrier, 0, nullptr);

      ////////////////// end recording
      VK_ASSERT( vkEndCommandBuffer(transferCmdBuffer) );

      VkPipelineStageFlags waitDst = VK_PIPELINE_STAGE_VERTEX_INPUT_BIT;
      VkSubmitInfo transferInfo = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext = nullptr,
        .waitSemaphoreCount = 0,
        .pWaitSemaphores = nullptr,
        .pWaitDstStageMask = &waitDst,
        .commandBufferCount = 1,
        .pCommandBuffers = &transferCmdBuffer,
        .signalSemaphoreCount = 0,
        .pSignalSemaphores = nullptr
      };
      // I'll opt for using an external subpass dependency in the render pass instead of waiting on a fence here
      // that means I can't immediately destroy the staging buffer, but I'll look into a more elegant automatic cleanup when
      // the gpu is done with the transfer
      
      // TODO I think I should just use a memory barrier instead

      VK_ASSERT( vkQueueSubmit(this->graphicsQueue, 1, &transferInfo, VK_NULL_HANDLE) );
    }


    void createRenderPass() {
      struct AttachmentInfo {
        VkAttachmentDescription description;
        VkAttachmentReference ref;
      };

      VkAttachmentDescription colorAttachmentDescription = {
        .flags = 0,
        .format = this->swapchainFormat.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,  // before rp
        .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, // auto-transition after rp
      };

      VkAttachmentReference colorAttRef {
          .attachment = 0,
          .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,  // auto-transition during rp
      };

      VkSubpassDescription subpassDescription = {
        .flags = 0,
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .inputAttachmentCount = 0,
        .pInputAttachments = nullptr,
        .colorAttachmentCount = 1,
        .pColorAttachments = &colorAttRef,
        .pResolveAttachments = nullptr,
        .pDepthStencilAttachment = nullptr,
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = nullptr
      };

      // TODO check if it's better to have explicit external dependencies

      VkRenderPassCreateInfo renderPassInfo = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .attachmentCount = 1,
        .pAttachments = &colorAttachmentDescription,
        .subpassCount = 1,
        .pSubpasses = &subpassDescription,
        .dependencyCount = 0,
        .pDependencies = nullptr
      };
      
      VK_ASSERT( vkCreateRenderPass(this->logicalDevice, &renderPassInfo, nullptr, &this->renderpass) );
    }

    void createSwapchain() {
      uint32_t numFormats;
      std::vector<VkSurfaceFormatKHR> formats;
      VK_ASSERT( vkGetPhysicalDeviceSurfaceFormatsKHR(this->physicalDevice, this->presentationSurface, &numFormats, nullptr) );
      formats.resize(numFormats);
      VK_ASSERT( vkGetPhysicalDeviceSurfaceFormatsKHR(this->physicalDevice, this->presentationSurface, &numFormats, formats.data()) );

      VkSurfaceFormatKHR desiredFormat = { .format = VK_FORMAT_B8G8R8_UNORM, .colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
      bool formatSupported = false;
      assert(formats.size() > 0);
      for (auto & available: formats) {
        if (available.format == desiredFormat.format && available.colorSpace == desiredFormat.colorSpace) {
          formatSupported = true;
          break;
        }
      } 
      VkSurfaceFormatKHR actualFormat = formatSupported ? desiredFormat : formats[0]; 

      VK_ASSERT( vkGetPhysicalDeviceSurfaceCapabilitiesKHR(this->physicalDevice, this->presentationSurface, &this->surfaceCapabilities) ); 

      VkSwapchainCreateInfoKHR swapchainInfo = {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .pNext = nullptr,
        .flags = 0,
        .surface = this->presentationSurface,
        .minImageCount = this->surfaceCapabilities.minImageCount,
        .imageFormat = actualFormat.format,
        .imageColorSpace = actualFormat.colorSpace,
        .imageExtent = window_size,
        .imageArrayLayers = 1,
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices = &this->presentFamily,
        .preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = VK_PRESENT_MODE_FIFO_KHR,  // TODO check mailbox
        .clipped = VK_FALSE,
        .oldSwapchain = this->currentSwapchain,
      };

      VkSwapchainKHR newSwapchain;
      VK_ASSERT( vkCreateSwapchainKHR(this->logicalDevice, &swapchainInfo, nullptr, &newSwapchain) );
      this->currentSwapchain = newSwapchain;
      this->swapchainFormat = actualFormat;

      uint32_t numImages;
      std::vector<VkImage> images;
      VK_ASSERT( vkGetSwapchainImagesKHR(this->logicalDevice, this->currentSwapchain, &numImages, nullptr) );
      images.resize(numImages);
      VK_ASSERT( vkGetSwapchainImagesKHR(this->logicalDevice, this->currentSwapchain, &numImages, images.data()) );

      // create image views
      for (uint32_t i = 0; i < images.size(); i++) {
        VkImageViewCreateInfo viewInfo = {
          .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
          .pNext = nullptr,
          .flags = 0,
          .image = images[i],
          .viewType = VK_IMAGE_VIEW_TYPE_2D,
          .format = this->swapchainFormat.format,
          .components = { VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY },
          .subresourceRange = { 
              .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
              .baseMipLevel = 0,
              .levelCount = VK_REMAINING_MIP_LEVELS,
              .baseArrayLayer = 0,
              .layerCount = VK_REMAINING_ARRAY_LAYERS
            },
        };

        VkImageView imageView;
        VK_ASSERT( vkCreateImageView(this->logicalDevice, &viewInfo, nullptr, &imageView) );
        this->swapchainImages.push_back({ images[i], imageView });
      }
    }


    uint32_t acquireImage(VkSemaphore imageAcquired) {
      uint32_t imageIndex;
      VkResult res = vkAcquireNextImageKHR(this->logicalDevice, this->currentSwapchain, 2000000000, 
              imageAcquired, VK_NULL_HANDLE, &imageIndex);
      assert( res == VK_SUCCESS || res == VK_SUBOPTIMAL_KHR );
      return imageIndex;
    }

    uint32_t createFramebuffer(uint32_t imageIndex, VkFramebuffer & framebuffer) {
      VkFramebufferCreateInfo fbInfo = {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .renderPass = this->renderpass,
        .attachmentCount = 1,
        .pAttachments = &swapchainImages[imageIndex].imageView,
        .width = window_size.width,
        .height = window_size.height,
        .layers = 1
      };

      VK_ASSERT( vkCreateFramebuffer(this->logicalDevice, &fbInfo, nullptr, &framebuffer) );
      return imageIndex;
    }

    VkShaderModule loadShaderModule(const std::string fname) {
      std::vector<unsigned char> data;
      FILE *f = fopen(fname.c_str(), "r");
      assert(f != nullptr);
      
      fseek(f, 0, SEEK_END);
      size_t len = ftell(f);
      fseek(f, 0, SEEK_SET);

      data.resize(len);
      assert( 1 == fread(data.data(), len, 1, f) );
      
      fclose(f);

      VkShaderModuleCreateInfo moduleInfo {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .codeSize = (uint32_t) data.size(),
        .pCode = (uint32_t *) data.data()  // TODO endianess?
      };

      VkShaderModule module;
      VK_ASSERT( vkCreateShaderModule(this->logicalDevice, &moduleInfo, nullptr, &module) );
      return module;
    }


    void createDrawingDescriptorSetLayout() {
      VkDescriptorSetLayoutBinding matrixBinding = {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .pImmutableSamplers = nullptr,
      };

      VkDescriptorSetLayoutCreateInfo descriptorSetLayoutInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .bindingCount = 1,
        .pBindings = &matrixBinding,
      };

      VK_ASSERT( vkCreateDescriptorSetLayout(this->logicalDevice, &descriptorSetLayoutInfo, nullptr, &this->drawDescriptorSetLayout) );
    }


    void createDrawingDescriptorPool() {
      VkDescriptorPoolSize poolSize = {
        .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = (uint32_t) this->swapchainImages.size(),
      };
 
      VkDescriptorPoolCreateInfo poolInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .maxSets = (uint32_t) this->swapchainImages.size(),
        .poolSizeCount = 1,
        .pPoolSizes = &poolSize
      };

      VK_ASSERT( vkCreateDescriptorPool(this->logicalDevice, &poolInfo, nullptr, &this->drawDescriptorPool) );
    }


    void createPipeline() {
      VkPipelineLayoutCreateInfo layoutInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .setLayoutCount = 1,
        .pSetLayouts = &this->drawDescriptorSetLayout,
        .pushConstantRangeCount = 0,
        .pPushConstantRanges = nullptr
      };

      VK_ASSERT( vkCreatePipelineLayout(this->logicalDevice, &layoutInfo, nullptr, &this->pipelineLayout) );

      VkShaderModule vertexModule = loadShaderModule("spirv/triangle.vert.spv");
      VkShaderModule fragModule = loadShaderModule("spirv/triangle.frag.spv");

      VkPipelineShaderStageCreateInfo vertexStage = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .stage = VK_SHADER_STAGE_VERTEX_BIT,
        .module = vertexModule,
        .pName = "main",
        .pSpecializationInfo = nullptr,
      };

      VkPipelineShaderStageCreateInfo fragStage = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
        .module = fragModule,
        .pName = "main",
        .pSpecializationInfo = nullptr,
      };

      std::vector<VkPipelineShaderStageCreateInfo> stages = { vertexStage, fragStage };

      ///////////// vertex input
      VkVertexInputBindingDescription vertexDescription = {
        .binding = 0,  // corresponds to buffer # in vkCmdBindBuffers
        .stride = vertex_stride,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,  // TODO check this
      };

      VkVertexInputAttributeDescription positionAttr = {
        .location = 0, // in shader ( location = 0 )
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = 0
      };
      VkVertexInputAttributeDescription colorAttr = {
        .location = 1,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = 3 * sizeof(float)
      };

      std::vector<VkVertexInputAttributeDescription> attrs = { positionAttr, colorAttr };

      VkPipelineVertexInputStateCreateInfo vertexInputInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .vertexBindingDescriptionCount = 1,
        .pVertexBindingDescriptions = &vertexDescription,
        .vertexAttributeDescriptionCount = (uint32_t) attrs.size(),
        .pVertexAttributeDescriptions = attrs.data(),
      };

      ////////////// input assembly
      VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE
      };
      
      ////////////// viewport + scissors
      VkViewport viewport = {
        .x = 0,
        .y = 0,
        .width = (float) window_size.width, // TODO verify this, might want to distinguish window, swapchain sizes
        .height = (float) window_size.height,
        .minDepth = 0.0f, 
        .maxDepth = 1.0f
      };
      
      VkRect2D scissors {
        .offset = {0, 0},
        .extent = window_size
      };

      VkPipelineViewportStateCreateInfo viewportInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .viewportCount = 1,
        .pViewports = &viewport,
        .scissorCount = 1,
        .pScissors = &scissors
      };

      //////////// rasterization
      VkPipelineRasterizationStateCreateInfo rasterizationInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_NONE,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .depthBiasConstantFactor = 0.0f,
        .depthBiasClamp = 0.0f,
        .depthBiasSlopeFactor = 0.0f,
        .lineWidth = 1.0f
      };

      ///////////// multisample
      VkPipelineMultisampleStateCreateInfo multisampleInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 0.0f,
        .pSampleMask = nullptr,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE
      };

      ///////////// color blend
      VkPipelineColorBlendAttachmentState attachmentState = {
        .blendEnable = VK_FALSE,
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
      };

      VkPipelineColorBlendStateCreateInfo blendInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .logicOpEnable = VK_FALSE,
        .attachmentCount = 1,
        .pAttachments = &attachmentState
      };

      VkGraphicsPipelineCreateInfo pipelineInfo = {
        VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        nullptr,
        0,
        (uint32_t) stages.size(),
        stages.data(),
        &vertexInputInfo,
        &inputAssemblyInfo,
        nullptr,
        &viewportInfo,
        &rasterizationInfo,
        &multisampleInfo,
        nullptr,
        &blendInfo,
        nullptr,
        this->pipelineLayout,
        this->renderpass,
        0,
        VK_NULL_HANDLE,
        -1
      };

      VK_ASSERT( vkCreateGraphicsPipelines(this->logicalDevice, VK_NULL_HANDLE, 1, &pipelineInfo, 
              nullptr, &this->pipeline) );

      vkDestroyShaderModule(this->logicalDevice, vertexModule, nullptr);
      vkDestroyShaderModule(this->logicalDevice, fragModule, nullptr);
    }


    VkCommandBuffer allocDrawingCommandBuffer() {
      VkCommandPoolCreateInfo poolInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        .queueFamilyIndex = this->graphicsFamily
      };
      VkCommandPool pool;
      VK_ASSERT( vkCreateCommandPool(this->logicalDevice, &poolInfo, nullptr, &pool) );

      VkCommandBufferAllocateInfo cmdBufferInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = nullptr,
        .commandPool = pool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
      };

      VkCommandBuffer drawingCmdBuffer;
      VK_ASSERT( vkAllocateCommandBuffers(this->logicalDevice, &cmdBufferInfo, &drawingCmdBuffer) );

      // will live throughout the application
      this->frameCommandPools.push_back(pool);
      return drawingCmdBuffer;
    }


    void destroyFrameResources() {
      for (uint32_t i = 0; i < this->frames.size(); i++) {
        FrameResources frame = this->frames[i];

        vkDestroyFramebuffer(this->logicalDevice, frame.framebuffer, nullptr);
        vkDestroyFence(this->logicalDevice, frame.finishedDrawing, nullptr);
        vkDestroySemaphore(this->logicalDevice, frame.imageAcquired, nullptr);
        vkDestroySemaphore(this->logicalDevice, frame.presentable, nullptr);
        vkUnmapMemory(this->logicalDevice, frame.uniformBuffer.memory);
        vkFreeMemory(this->logicalDevice, frame.uniformBuffer.memory, nullptr);
        vkDestroyBuffer(this->logicalDevice, frame.uniformBuffer.buffer, nullptr);
      }
    }


    std::vector<BufferResources> allocateUniformBuffers(uint32_t numBuffers) {
      std::vector<BufferResources> buffers;
      buffers.resize(numBuffers);

      // will alloc individually for now
      for (uint32_t i = 0; i < numBuffers; i++) {
        createBuffer(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, sizeof(glm::mat4),
                buffers[i].buffer, buffers[i].memory);
        
        VK_ASSERT( vkMapMemory(this->logicalDevice, buffers[i].memory, 0, VK_WHOLE_SIZE, 0, &buffers[i].mappedData) );
      }
      return buffers;
    }


    std::vector<VkDescriptorSet> createDescriptorSets(std::vector<BufferResources> buffers) {
      uint32_t numSets = buffers.size();
      std::vector<VkDescriptorSet> sets;
      sets.resize(numSets);

      std::vector<VkDescriptorSetLayout> layoutRepeat;
      for (uint32_t i = 0; i < numSets; i++)
        layoutRepeat.push_back(this->drawDescriptorSetLayout);

      VkDescriptorSetAllocateInfo setAllocInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .pNext = nullptr,
        .descriptorPool = this->drawDescriptorPool,
        .descriptorSetCount = numSets,
        .pSetLayouts = layoutRepeat.data()
      };

      VK_ASSERT( vkAllocateDescriptorSets(this->logicalDevice, &setAllocInfo, sets.data()) );

      std::vector<VkWriteDescriptorSet> writes;
      for (uint32_t i = 0; i < sets.size(); i++) {
        VkDescriptorBufferInfo bufferInfo = {
          .buffer = buffers[i].buffer,
          .offset = 0,
          .range = sizeof(glm::mat4)
        };

        VkWriteDescriptorSet writeInfo = {
          .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
          .dstSet = sets[i],
          .dstBinding = 0,
          .dstArrayElement = 0,
          .descriptorCount = 1,
          .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
          .pBufferInfo = &bufferInfo
        };

        writes.push_back(writeInfo);
      }

      vkUpdateDescriptorSets(this->logicalDevice, writes.size(), writes.data(), 0, nullptr);

      return sets;
    }


    void initFrameResources() {
      this->frames.resize(std::min((uint32_t) swapchainImages.size(), maxFrames));

      VkFenceCreateInfo fenceInfo = {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .pNext = nullptr,
        .flags = VK_FENCE_CREATE_SIGNALED_BIT
      };
      VkSemaphoreCreateInfo semInfo = {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
      };

      std::vector<BufferResources> uniformBuffers = allocateUniformBuffers(this->frames.size());
      std::vector<VkDescriptorSet> descriptorSets = createDescriptorSets(uniformBuffers);

      for (uint32_t i = 0; i < this->frames.size(); i++) {
        FrameResources currentFrame;
        VK_ASSERT( vkCreateFence(this->logicalDevice, &fenceInfo, nullptr, &currentFrame.finishedDrawing) );
        VK_ASSERT( vkCreateSemaphore(this->logicalDevice, &semInfo, nullptr, &currentFrame.imageAcquired) );
        VK_ASSERT( vkCreateSemaphore(this->logicalDevice, &semInfo, nullptr, &currentFrame.presentable) );
        currentFrame.commandBuffer = allocDrawingCommandBuffer();
        currentFrame.uniformBuffer = uniformBuffers[i];
        currentFrame.framebuffer = VK_NULL_HANDLE;
        currentFrame.descriptorSet = descriptorSets[i];
        frames[i] = currentFrame;
      }
    }

    void resetFrame(FrameResources & frame) {
      VK_ASSERT( vkResetFences(this->logicalDevice, 1, &frame.finishedDrawing) );
      vkDestroyFramebuffer(this->logicalDevice, frame.framebuffer, nullptr);
      vkResetCommandBuffer(frame.commandBuffer, 0);
    }


    void updatePerFrameData(BufferResources buffer) {
      static auto startTime = std::chrono::steady_clock::now();

      auto timeNow = std::chrono::steady_clock::now();
      std::chrono::duration<float> fsec = timeNow - startTime;

      glm::mat4 transformation;

      glm::mat4 model = glm::identity<glm::mat4>();
      glm::vec3 rotAxis = glm::normalize(glm::vec3(0.0f, 1.0f, 0.0f));
      model = glm::rotate(model, 2 * (float) M_PI * fsec.count(), rotAxis);

      glm::mat4 view = glm::identity<glm::mat4>();
      view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));

      glm::mat4 projection;
      float aspect = ((float)window_size.width) / window_size.height;
      projection = glm::perspective(glm::radians(45.0f), aspect, 0.1f, 100.0f); 

      transformation = projection * view * model;

      VkMappedMemoryRange range = {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .memory = buffer.memory,
        .offset = 0,
        .size = sizeof(transformation)
      };

      memcpy(buffer.mappedData, &transformation[0], sizeof(transformation));
      VK_ASSERT( vkFlushMappedMemoryRanges(this->logicalDevice, 1, &range) );
    }


    void drawFrame(FrameResources & resources) {
      resources.imageIndex = acquireImage(resources.imageAcquired);
      createFramebuffer(resources.imageIndex, resources.framebuffer);

      VkCommandBufferBeginInfo recordingBeginInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = nullptr,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        .pInheritanceInfo = nullptr
      };

      VkClearValue clearVal;
      clearVal.color = {{ 0.0f, 0.0f, 0.0f, 1.0f }};
      VkRenderPassBeginInfo rpBeginInfo = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .pNext = nullptr,
        .renderPass = this->renderpass,
        .framebuffer = resources.framebuffer,
        .renderArea = { {0, 0}, window_size },
        .clearValueCount = 1,
        .pClearValues = &clearVal,
      };

      updatePerFrameData(resources.uniformBuffer);

      ////////// begin recording
      VK_ASSERT( vkBeginCommandBuffer(resources.commandBuffer, &recordingBeginInfo) );

      vkCmdBeginRenderPass(resources.commandBuffer, &rpBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

      vkCmdBindPipeline(resources.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, this->pipeline);
      
      VkDeviceSize off = 0;
      vkCmdBindVertexBuffers(resources.commandBuffer, 0, 1, &this->vertexBuffer, &off); 

      vkCmdBindDescriptorSets(resources.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, this->pipelineLayout,
              0, 1, &resources.descriptorSet, 0, nullptr);

      vkCmdDraw(resources.commandBuffer, 3, 1, 0, 0);

      vkCmdEndRenderPass(resources.commandBuffer);

      ////////// end recording
      VK_ASSERT( vkEndCommandBuffer(resources.commandBuffer) );

      VkPipelineStageFlags dstMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
      VkSubmitInfo submitInfo = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext = nullptr,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &resources.imageAcquired,
        .pWaitDstStageMask = &dstMask,
        .commandBufferCount = 1,
        .pCommandBuffers = &resources.commandBuffer,
        .signalSemaphoreCount = 1,
        .pSignalSemaphores = &resources.presentable
      };
      vkQueueSubmit(this->graphicsQueue, 1, &submitInfo, resources.finishedDrawing);

      VkPresentInfoKHR presentInfo = {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .pNext = nullptr,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &resources.presentable,
        .swapchainCount = 1,
        .pSwapchains = &this->currentSwapchain,
        .pImageIndices = &resources.imageIndex,
        .pResults = nullptr
      };

      VK_ASSERT( vkQueuePresentKHR(this->presentQueue, &presentInfo) );
    }
};

Context::Context() {
  instance = VK_NULL_HANDLE;
  physicalDevice = VK_NULL_HANDLE;
  logicalDevice = VK_NULL_HANDLE;
  currentSwapchain = VK_NULL_HANDLE;
  pipelineLayout = VK_NULL_HANDLE;
  pipeline = VK_NULL_HANDLE;
}

void Context::renderLoop() {
  
  uint32_t framePos = 0;
  while ( !glfwWindowShouldClose(this->window) ) {
    glfwPollEvents();
    FrameResources & currentFrame = this->frames[framePos];
    VK_ASSERT( vkWaitForFences(this->logicalDevice, 1, &currentFrame.finishedDrawing, VK_FALSE, 2000000000) );
    
    resetFrame(currentFrame);

    // for now dispatch the frame here and wait until it's recorded
    drawFrame(currentFrame);

    framePos = (framePos + 1) % this->frames.size();
  }

  glfwDestroyWindow(this->window);
  glfwTerminate();
}

void Context::init() {
  initGLFW();

  loadGlobalFunctions();
  createInstance();
  loadInstanceFunctions();
  createWindow();
  createSurface();
  assert(choosePhysicalDeviceAndQueueFamilies());
  createLogicalDevice();
  loadDeviceFunctions();
  getQueues();

  createSwapchain();

  getMemoryProperties();
  createStagingBuffer();
  uploadToVertexStagingBuffer();
  createVertexBuffer();
  oneTimeVertexTransfer();

  createRenderPass();
  createDrawingDescriptorSetLayout();
  createPipeline();

  createDrawingDescriptorPool();

  initFrameResources();
}

void Context::shutdown() {
  vkDeviceWaitIdle(logicalDevice);

  destroyFrameResources();

  vkResetDescriptorPool(logicalDevice, this->drawDescriptorPool, 0);
  vkDestroyDescriptorPool(logicalDevice, drawDescriptorPool, nullptr);
  vkDestroyPipeline(logicalDevice, pipeline, nullptr);
  vkDestroyPipelineLayout(logicalDevice, pipelineLayout, nullptr);
  vkDestroyDescriptorSetLayout(logicalDevice, drawDescriptorSetLayout, nullptr);

  for (SwapchainImageResources info: swapchainImages)
    if (info.imageView != VK_NULL_HANDLE)
      vkDestroyImageView(logicalDevice, info.imageView, nullptr);
  vkDestroySwapchainKHR(logicalDevice, currentSwapchain, nullptr);

  for (VkCommandPool pool: frameCommandPools) {
    vkResetCommandPool(logicalDevice, pool, VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);
    vkDestroyCommandPool(logicalDevice, pool, nullptr);
  }

  vkDestroyRenderPass(logicalDevice, renderpass, nullptr);

  vkResetCommandPool(logicalDevice, transferPool, VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);
  vkDestroyCommandPool(logicalDevice, transferPool, nullptr);

  vkFreeMemory(logicalDevice, vertexBufferMemory, nullptr);
  vkDestroyBuffer(logicalDevice, vertexBuffer, nullptr);
  vkFreeMemory(logicalDevice, vertexStagingMemory, nullptr);
  vkDestroyBuffer(logicalDevice, vertexStagingBuffer, nullptr);
  vkDestroyDevice(logicalDevice, nullptr);
  vkDestroySurfaceKHR(instance, presentationSurface, nullptr);
  vkDestroyInstance(instance, nullptr);
  dlclose(library);
}

} // namespace VkTriangle



using namespace VkTriangle;

int main() {
  Context appContext;
  appContext.init();

  appContext.renderLoop();

  appContext.shutdown();
}